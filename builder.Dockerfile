FROM registry.gitlab.com/texttoconvert/tools/python-pandoc-texlive:latest

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY ./ /app/

RUN ./scripts/get_documentation.sh

RUN ./scripts/build_documentation.sh yes
