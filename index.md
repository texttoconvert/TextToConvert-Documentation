# Introduction

- [Introduction](#introduction)
  - [What is TextToConvert](#what-is-texttoconvert)
  - [How does it work](#how-does-it-work)

## What is TextToConvert

TextToConvert is a web application for document composition.
Its main purpose is to allow users to write documentation from different languages to different types of document output.
It is intended to be free to use, improve and share.

## How does it work

TextToConvert is a web application with an architecture based on micro-services.

TextToConvert has an independant machine to human interface, HMI, from the rest of the whole application.
This HMI is meant to be user-friendly and simple to use.
It contains both an editor, for the user to write is content, and a preview of the output document.

TextToConvert also has a back-end.
The back-end is responsible for compiling the input document to the output document.
