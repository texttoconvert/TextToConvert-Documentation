#!/bin/bash

#
# This script get the functionnal and technical documentation from other TextToConvert repositories
# This specific documentation is then added to the full documentation
#

BRANCH="master"
EXTENSION="zip"
DOCUMENTATION_PATH="documentation"
WEB_FOLDER="web_folder"

MAIN=( "TextToConvert-Front" "TextToConvert-Back" )

TOOLS=( "python-pandoc-texlive" )

PARENTS=( "main" "tools" )

mkdir ${WEB_FOLDER}
rm -rf ${WEB_FOLDER}/**
find . -regextype awk -regex "(.*/.*\.md|.*/.*\.html)" -exec cp "{}" ${WEB_FOLDER}/ \;

set +e

for parent in "${PARENTS[@]}"; do

    echo "Get documentation from group ${parent} ..."
    if [ "${parent}" == "main" ]; then
        PATHS=("${MAIN[@]}")
    elif [ "${parent}" == "tools" ]; then
        PATHS=("${TOOLS[@]}")
    fi

    for path in "${PATHS[@]}"; do
        echo "       Get documentation from repository ${path}"
        mkdir "${path}"
        url="https://gitlab.com/texttoconvert/${parent}/${path}/-/jobs/artifacts/${BRANCH}/download?job=${DOCUMENTATION_PATH}"
        wget "${url}" -O "${path}.${EXTENSION}"
        unzip "${path}.${EXTENSION}" -d "${path}"

        if [ ! -d "${path}/${DOCUMENTATION_PATH}" ] ;then
            mkdir ${path}/${DOCUMENTATION_PATH}
        fi

        echo "       Get ${path} README"
        readme_url="https://gitlab.com/texttoconvert/${parent}/${path}/raw/${BRANCH}/README.md?inline=false"
        wget "${readme_url}" -O "${path}/${DOCUMENTATION_PATH}/README.md"
        mkdir "${WEB_FOLDER}/${path}"
        mv "${path}/${DOCUMENTATION_PATH}/" "${WEB_FOLDER}/${path}/"
        rm -rf "${path}"

    done
    echo "... documentation from group ${parent} has been added"

done

mkdir "${WEB_FOLDER}/POC"
wget "https://gitlab.com/texttoconvert/TextToConvert-POC/raw/master/README.md?inline=false" -O "${WEB_FOLDER}/POC/README.md"

rm *.${EXTENSION}

set -e

