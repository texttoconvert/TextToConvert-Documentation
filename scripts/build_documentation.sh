#!/bin/bash

#
# This script builds the full documentation
#

DOCUMENTATION_PATH="./web_folder"
NAVBAR_FILE="NAVBAR"
NAVBAR_PATH=${DOCUMENTATION_PATH}/${NAVBAR_FILE}
SUMMARY_FILE="SUMMARY"
FOOTER="./footer.html"

set +e
rm -rf ./public
set -e

# create navbar
for summary_file in $(find ${DOCUMENTATION_PATH} -name ${SUMMARY_FILE}'.md' -exec bash -c 'printf "%s\n" "${@%.*}"' _ {} +)
do
    echo "" >> "${NAVBAR_PATH}".md
    cat ${summary_file}'.md' >> "${NAVBAR_PATH}".md
    rm "${summary_file}".md
done
pandoc "${NAVBAR_PATH}".md --from=markdown --to=html --output="${NAVBAR_PATH}".html
rm "${NAVBAR_PATH}".md
echo -e "<nav>\n$(cat ${NAVBAR_PATH}.html)\n</nav>" > "${NAVBAR_PATH}".html

# create standalone
cp ${FOOTER} ${DOCUMENTATION_PATH}
for md_file in $(find ${DOCUMENTATION_PATH} -name '*.md' -exec bash -c 'printf "%s\n" "${@%.*}"' _ {} +)
do
    echo "file ${md_file}"
    pandoc "${md_file}".md --from=markdown --to=html --output="${md_file}".html
    echo -e "<div id='main'>\n$(cat ${md_file}.html)\n</div>" > "${md_file}".html
    pandoc "${md_file}".html --metadata=title:"TextToConvert" --metadata=css:"/TextToConvert-Documentation/style/main.css" \
        --from=html --to=html --output="${md_file}".html -s -B "${NAVBAR_PATH}".html -A "${DOCUMENTATION_PATH}/${FOOTER}"
    rm "${md_file}".md
done

# clean
cp -r style/ ${DOCUMENTATION_PATH}
mv ${DOCUMENTATION_PATH} public

if [ -n "$1" ] ; then
    echo "Change root for local documentation"
    find ./public/ -name "*.html" -exec sed -i 's/\/TextToConvert-Documentation\//\//g' {} \;
fi