# Building documentation

To build locally the documentation you can use `docker-compose`:

```sh
docker-compose down -v # to remove all docker-compose created volumes
docker-compose up --build --force-recreate # to force images build
```
